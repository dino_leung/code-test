/*jshint loopfunc: true */

module.exports = function(app) {
  var User = app.models.User;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  var Team = app.models.Team;

  var adminEmail = [
    'admin0@admin.com',
    'admin1@admin.com',
    'admin2@admin.com'
  ];


  //create the admin role
  Role.findOne({where: {name: 'admin'}}, function(err, role){
    if (err) throw err;
    if (null === role){
      console.log('not found');
      Role.create({
        name: 'admin'
      }, function(err, newRole) {
        if (err) throw err;
        console.log('Created role:', newRole);
        assignAdmin(newRole);
      });
    }
    else{
      console.log('admin role already exist');
      assignAdmin(role);
    }
  });

//make user in adminEmail list into admin
  function assignAdmin(role){
    for (var i in adminEmail){
      User.findOne({where: {email: adminEmail[i]}}, function(err, user){
        if (err) throw err;
        if (null === user)
          console.log('user: ' + adminEmail[i] + 'not found');
        else{
          RoleMapping.findOne({where: {principalId: user.id, roleId: role.id}}, function(err, admin){
            if (err) throw err;
            if (null === admin){
              console.log('make ' + user.email + ' an admin');
              role.principals.create({
                principalType: RoleMapping.USER,
                principalId: user.id
              }, function(err, principal){
                if (err) throw err;
                  console.log('Created principal:', principal);
              });
            }
            else
              console.log(user.email + ' is already an admin');

          });
        }
      });
    }
  }
};