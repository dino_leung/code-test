module.exports = function(grunt) {
	grunt.initConfig({
		//grunt tasks go here

		jshint: {
			all: ['common/models/*.js', 'server/boot/adminRole.js']
		},

		concat: {
			options: {
				separator: ';'
			},
			dist: {
				src: ['common/models/*.js', 'server/boot/adminRole.js'],
				dest:'./script.js'
			}
		},

		uglify: {
			js: {
				files: {
					'./script.js': ['./script.js']
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify']);
}