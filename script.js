var loopback = require("loopback"),
    deepEqual = require("deep-equal"),
    fs = require("fs"),
    async = require("async"),
    stream = require("stream"),
    pkgcloud = require("pkgcloud");
pkgcloud.providers.filesystem = {}, pkgcloud.providers.filesystem.storage = require("filesystem-storage-pkgcloud");
var client = null,
    containerName = "test-container",
    basePath = "storage",
    path = basePath + "/" + containerName + "/";
module.exports = function(a) {
    a.upload = function(a, b) {
        if (void 0 === a.req.accessToken) b("access token not found");
        else if (0 === a.req.files.length) b("image file not found");
        else if (void 0 === a.req.body.label) b("label not found");
        else {
            void 0 === a.req.body["public"] && (a.req.body["public"] = !0);
            var c, d = a.req.files[0],
                e = d.mimetype.split("/"),
                f = {
                    label: a.req.body.label,
                    "public": a.req.body["public"],
                    comments: a.req.body.comments,
                    attributes: a.req.body.attributes,
                    tags: a.req.body.tags,
                    views: 0,
                    imageType: e[1],
                    userId: a.req.accessToken.userId
                };
            console.log(e), "image" !== e[0] ? b("file uploaded has to be an image") : this.create(f, function(a, f) {
                c = f.id + "." + e[1], client = pkgcloud.storage.createClient({
                    provider: "filesystem",
                    root: basePath
                }), client.createContainer(containerName);
                var g = client.upload({
                        container: containerName,
                        remote: c
                    }),
                    h = new stream.Readable;
                h.push(d.buffer), h.push(null), h.pipe(g), g.on("error", function(a) {
                    console.log("Error")
                }), g.on("success", function(a) {
                    console.log("It works "), fs.readFile(path + c, function(a, b) {
                        a && console.log("Error")
                    })
                }), b(a, f)
            })
        }
    }, a.remoteMethod("upload", {
        description: "Uploads an image",
        http: {
            path: "/upload",
            verb: "post"
        },
        returns: {
            arg: "data",
            type: "object",
            root: !0
        },
        accepts: {
            arg: "ctx",
            type: "object",
            http: {
                source: "context"
            }
        }
    }), a.view = function(b, c, d) {
        async.waterfall([
            function(c) {
                var d;
                void 0 === b.req.accessToken ? c(null, "undefined", !1) : (d = b.req.accessToken.userId, a.app.models.Role.findOne({
                    where: {
                        name: "admin"
                    }
                }, function(b, e) {
                    if (b) throw b;
                    null === e ? console.log("role not found") : a.app.models.RoleMapping.findOne({
                        where: {
                            principalId: d,
                            roleId: e.id
                        }
                    }, function(a, b) {
                        if (a) throw a;
                        c(null, d, null !== b)
                    })
                }))
            },
            function(b, d, e) {
                void 0 === c ? e(null, "imageId") : a.findById(c, function(a, c) {
                    a && e(a), null === c ? e(null, "noImage") : c["public"] || d || b === c.userId ? (c.views += 1, c.save(function(a, b) {
                        a ? e(a) : e(null, c.id + "." + c.imageType)
                    })) : e(null, "notPublic")
                })
            }
        ], function(a, c) {
            if ("imageId" === c) d("image id not found");
            else if ("noImage" === c) d("no such image");
            else if ("notPublic" === c) d("image is not public");
            else {
                var e = {
                    root: path,
                    dotfiles: "deny",
                    headers: {
                        "x-timestamp": Date.now(),
                        "x-sent": !0
                    }
                };
                b.res.sendFile(c, e, function(a) {
                    a ? (console.log(a), b.res.status(a.status).end()) : console.log("Sent:", c)
                })
            }
        })
    }, a.remoteMethod("view", {
        description: "View an image",
        http: {
            path: "/view",
            verb: "get"
        },
        returns: {
            arg: "data",
            type: "object",
            root: !0
        },
        accepts: [{
            arg: "ctx",
            type: "object",
            http: {
                source: "context"
            }
        }, {
            arg: "imageId",
            type: "string"
        }]
    }), a.edit = function(a, b, c, d, e, f, g, h) {
        void 0 === a.req.accessToken ? h("access token not found") : void 0 === b ? h("image id not found") : this.findById(b, function(b, i) {
            if (b) h(b);
            else if (a.req.accessToken.userId !== i.userId) h("you are not the owner");
            else {
                0 !== c.length && (i.attributes = c), void 0 !== d && (i.comments = d), void 0 !== e && (i.label = e), void 0 !== f && (i["public"] = f), 0 !== g.length && (i.tags = g), i.save(function(a, b) {
                    a ? h(a) : h(null, b)
                })
            }
        })
    }, a.remoteMethod("edit", {
        description: "Edit an image",
        http: {
            path: "/edit",
            verb: "post"
        },
        returns: {
            arg: "data",
            type: "object",
            root: !0
        },
        accepts: [{
            arg: "ctx",
            type: "object",
            http: {
                source: "context"
            }
        }, {
            arg: "imageId",
            type: "string"
        }, {
            arg: "attributes",
            type: "array"
        }, {
            arg: "comments",
            type: "string"
        }, {
            arg: "label",
            type: "string"
        }, {
            arg: "public",
            type: "boolean"
        }, {
            arg: "tags",
            type: "array"
        }]
    })
}, module.exports = function(a) {
    a.images = function(b, c, d) {
        if (void 0 === c) void 0 === b.req.accessToken ? d("no user id or user token found") : a.app.models.image.find({
            where: {
                userId: b.req.accessToken.userId
            }
        }, function(a, b) {
            a ? d(a) : d(null, b)
        });
        else {
            var e;
            e = void 0 === b.req.accessToken ? !0 : b.req.accessToken.userId !== c, e ? a.app.models.image.find({
                where: {
                    userId: c,
                    "public": !0
                }
            }, function(a, b) {
                a ? d(a) : d(null, b)
            }) : a.app.models.image.find({
                where: {
                    userId: c
                }
            }, function(a, b) {
                a ? d(a) : d(null, b)
            })
        }
    }, a.remoteMethod("images", {
        description: "View user's all images",
        http: {
            path: "/images",
            verb: "get"
        },
        returns: {
            arg: "data",
            type: "object",
            root: !0
        },
        accepts: [{
            arg: "ctx",
            type: "object",
            http: {
                source: "context"
            }
        }, {
            arg: "uid",
            type: "string"
        }]
    }), a.views = function(b, c, d) {
        void 0 === c ? void 0 === b.req.accessToken ? d("no user id or user token found") : a.app.models.image.find({
            where: {
                userId: b.req.accessToken.userId
            }
        }, function(a, b) {
            if (a) d(a);
            else {
                var c = 0;
                for (i = 0; i < b.length; i++) c += b[i].views;
                d(null, c)
            }
        }) : a.app.models.image.find({
            where: {
                userId: c
            }
        }, function(a, b) {
            if (a) d(a);
            else {
                var c = 0;
                for (i = 0; i < b.length; i++) c += b[i].views;
                d(null, c)
            }
        })
    }, a.remoteMethod("views", {
        description: "View user's total views",
        http: {
            path: "/views",
            verb: "get"
        },
        returns: {
            arg: "data",
            type: "object",
            root: !0
        },
        accepts: [{
            arg: "ctx",
            type: "object",
            http: {
                source: "context"
            }
        }, {
            arg: "uid",
            type: "string"
        }]
    })
}, module.exports = function(a) {
    function b(a) {
        for (var b in f) c.findOne({
            where: {
                email: f[b]
            }
        }, function(c, d) {
            if (c) throw c;
            null === d ? console.log("user: " + f[b] + "not found") : e.findOne({
                where: {
                    principalId: d.id,
                    roleId: a.id
                }
            }, function(b, c) {
                if (b) throw b;
                null === c ? (console.log("make " + d.email + " an admin"), a.principals.create({
                    principalType: e.USER,
                    principalId: d.id
                }, function(a, b) {
                    if (a) throw a;
                    console.log("Created principal:", b)
                })) : console.log(d.email + " is already an admin")
            })
        })
    }
    var c = a.models.User,
        d = a.models.Role,
        e = a.models.RoleMapping,
        f = (a.models.Team, ["admin0@admin.com", "admin1@admin.com", "admin2@admin.com"]);
    d.findOne({
        where: {
            name: "admin"
        }
    }, function(a, c) {
        if (a) throw a;
        null === c ? (console.log("not found"), d.create({
            name: "admin"
        }, function(a, c) {
            if (a) throw a;
            console.log("Created role:", c), b(c)
        })) : (console.log("admin role already exist"), b(c))
    })
};