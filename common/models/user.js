module.exports = function(User) {

	//get all img of a user
	User.images = function(ctx, uid, cb){

		if (null === uid){
			// cb('no uid');
			if (null === ctx.req.accessToken)
				cb('no user id or user token found');
			else{
				// cb(null, 'get own images');
				User.app.models.image.find({where: {userId: ctx.req.accessToken.userId}}, function(err, instance){
					if (err)
						cb(err);
					else
						cb(null, instance);
				});
			}
		}
		else{
			// cb(null, 'get uid images');
			var notOwner;
			if (null === ctx.req.accessToken)
				notOwner = true;
			else
				notOwner = ctx.req.accessToken.userId !== uid;
			if (notOwner)
				User.app.models.image.find({where: {userId: uid, public: true}}, function(err, instance){
						if (err)
							cb(err);
						else
							cb(null, instance);
					});
			else
				User.app.models.image.find({where: {userId: uid}}, function(err, instance){
						if (err)
							cb(err);
						else
							cb(null, instance);
					});
		}
			
		// cb(null, 'ok');
	};
	User.remoteMethod('images',
		{
	    	description: 'View user\'s all images',
	     	http: {path: '/images', verb: 'get'},
	     	returns: {arg: 'data', type: 'object', root: true},
	     	accepts: [
	     				{arg: 'ctx', type: 'object', http: { source: 'context' } },
	     				{arg: 'uid', type: 'string'}
	     			]
	    }
	);

	//TODO get total views of a user
	User.views = function(ctx, uid, cb){

		if (undefined === uid){
			// cb('no uid');
			if (null === ctx.req.accessToken)
				cb('no user id or user token found');
			else{
				//get own count
				User.app.models.image.find({where: {userId: ctx.req.accessToken.userId}}, function(err, instance){
					if (err)
						cb(err);
					else{
						// console.log(instance[0].views);
						var count = 0;
						for (i = 0; i< instance.length; i++)
							count += instance[i].views;
						cb(null, count);
					}
				});
			}
		}
		else{
			//get user count
			User.app.models.image.find({where: {userId: uid}}, function(err, instance){
				if (err)
					cb(err);
				else{
					var count = 0;
					for (i = 0; i< instance.length; i++)
							count += instance[i].views;
					cb(null, count);
				}
			});
		}

	};
	User.remoteMethod('views',
		{
	    	description: 'View user\'s total views',
	     	http: {path: '/views', verb: 'get'},
	     	returns: {arg: 'data', type: 'object', root: true},
	     	accepts: [
	     				{arg: 'ctx', type: 'object', http: { source: 'context' } },
	     				{arg: 'uid', type: 'string'}
	     			]
	    }
	);
};
