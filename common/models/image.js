var loopback = require('loopback');
var deepEqual = require('deep-equal');
var fs = require('fs');
var async = require('async');
var stream = require('stream');
var pkgcloud = require('pkgcloud');
pkgcloud.providers.filesystem = {};
pkgcloud.providers.filesystem.storage = require('filesystem-storage-pkgcloud');

var client = null;
var containerName = 'test-container';
var basePath = 'storage';

var path = basePath + '/' + containerName + '/';

module.exports = function(Image) {

	//upload an image with attributes
	Image.upload = function(ctx, cb){

		//check if necessary info is exist
		if (null === ctx.req.accessToken)
			cb('access token not found');
		else if (ctx.req.files === undefined)
			cb('image file not found');
		else if (undefined === ctx.req.body.label)
			cb('label not found');
		else {
			//public by default
			if (undefined === ctx.req.body.public)
				ctx.req.body.public = true;

			//create image data
			var imageFile = ctx.req.files[0];
			var imageMeta = imageFile.mimetype.split('/');
			var fileName;
			var imageData = {
				label: ctx.req.body.label,
				'public': ctx.req.body.public,
				comments: ctx.req.body.comments,
				attributes: ctx.req.body.attributes,
				tags: ctx.req.body.tags,
				views: 0,
				imageType: imageMeta[1],
				userId: ctx.req.accessToken.userId,
			};

			console.log(imageMeta);
			if(imageMeta[0] !== 'image')
				cb('file uploaded has to be an image');
			else{
				// create entry in the db
				this.create(imageData, function(err, instance) {

					//save img into local storage
					fileName = instance.id + '.' + imageMeta[1];
					client = pkgcloud.storage.createClient({
					    provider: 'filesystem',
					    root: basePath,
					});
					client.createContainer(containerName);
					var uploadStream = client.upload({
					    container: containerName,
					    remote: fileName,
					});
					var file = new stream.Readable();
					file.push(imageFile.buffer);
					file.push(null);
					file.pipe(uploadStream);
					uploadStream.on('error', function(err) {
					    console.log('Error');
					});

					uploadStream.on('success', function(file) {
					    console.log('It works ');

					    fs.readFile(path + fileName, function(err, data) {
					    	if (err) console.log('Error');
					    });
					});

				    cb(err, instance);
				});
			}
		}
		
	};
	Image.remoteMethod( 'upload',
	    {
	    	description: 'Uploads an image',
	     	http: {path: '/upload', verb: 'post'},
	     	returns: {arg: 'data', type: 'object', root: true},
	     	accepts: {arg: 'ctx', type: 'object', http: { source: 'context' } }
	    }
	);

	//view and image with image id
	//return image if the image is public or is owner
	Image.view = function(ctx, imageId, cb){
		

		async.waterfall([
			function(cb){
				var uid;
				if (null === ctx.req.accessToken)
					cb(null, 'undefined', false);
				else{
					uid = ctx.req.accessToken.userId;
					//check if user is admin
					Image.app.models.Role.findOne({where: {name: 'admin'}}, function(err, role){
						if (err) throw err;
						if (null === role)
		      				console.log('role not found');
		      			else{
		      				Image.app.models.RoleMapping.findOne({where: {principalId: uid, roleId: role.id}}, function(err, admin){
								if (err) throw err;
								cb(null, uid, (admin !== null));
							});
		      			}
					});
				}
			},
			function(uid, isAdmin, cb){
				if (undefined ===  imageId)
					cb(null, 'imageId');
				else{
					Image.findById(imageId, function(err, instance){
						if (err)
							cb(err);
						if (null === instance)
							cb(null, 'noImage');
						else if (instance.public || 
							isAdmin ||
							uid === instance.userId){
							//count views
							instance.views += 1;
							instance.save(function(err, items){
								if (err)
									cb(err);
								else{
									cb(null, instance.id + '.' + instance.imageType);
								}
							});
						}
						else
							cb(null, 'notPublic');
					});
					
				}
			}
		], function(err, fileName) {
			if (fileName === 'imageId')
				cb('image id not found');
			else if (fileName === 'noImage')
				cb('no such image');
			else if (fileName === 'notPublic')
				cb('image is not public');
			else{
				var options = {
					root: path,
					dotfiles: 'deny',
					headers: {
						'x-timestamp': Date.now(),
						'x-sent': true
					}
				};
				ctx.res.sendFile(fileName, options, function (err) {
					if (err) {
						console.log(err);
						ctx.res.status(err.status).end();
					}
					else {
						console.log('Sent:', fileName);
					}
				});
			}
		});
	};
	Image.remoteMethod( 'view',
		{
			description: 'View an image',
			http: {path: '/view', verb: 'get'},
			returns: {arg: 'data', type: 'object', root: true},
			accepts: [{arg: 'ctx', type: 'object', http: {source: 'context'}},
						{arg: 'imageId', type: 'string'}]
					
		}
	);

	//edit image meta data
	//chenge if there is any
	Image.edit = function(ctx, imageId, attributes, comments, label, public, tags, cb){

		if (undefined === ctx.req.accessToken)
			cb('access token not found');
		else if(undefined === imageId)
			cb('image id not found');
		else{
			this.findById(imageId, function(err, instance){
				if (err)
					cb(err);
				else{
					if (ctx.req.accessToken.userId !== instance.userId)
						cb('you are not the owner');
					else{
						var org = instance;
						if (attributes.length !== 0)
							instance.attributes = attributes;
						if (undefined !== comments)
							instance.comments = comments;
						if (undefined !== label)
							instance.label = label;
						if (undefined !== public)
							instance.public	= public;
						if (tags.length !== 0)
							instance.tags = tags;

						instance.save(function(err, items){
							if (err)
								cb(err);
							else
								cb(null, items);
						});
					}
					
				}
			});
		}

	};
	Image.remoteMethod( 'edit',
		{
			description: 'Edit an image',
			http: {path: '/edit', verb: 'post'},
			returns: {arg: 'data', type: 'object', root: true},
			accepts: [{arg: 'ctx', type: 'object', http: {source: 'context'}},
						{arg: 'imageId', type: 'string'},
						{arg: 'attributes', type: 'array'},
						{arg: 'comments', type: 'string'},
						{arg: 'label', type: 'string'},
						{arg: 'public', type: 'boolean'},
						{arg: 'tags', type: 'array'}]
					
		}
	);
};
